# Whole process listed below.
# .Read and filter raw data (input: csv file of raw acquired data / output: two vectors of selected data for right and left turns)
# .Calculate stress using models for each row of data (input: vector / output: vector of stress mean and amplitude)
# ...Dynamics of a mass point
# ...Arrangement of forces from tire contact point to wheel center bolt interfaces
# ...Internal forces and stresses on a curved beam
# .Fit a distribution for stress for each turn. (input: vector of stress data / output: stress distribution parameters)
# .Run FORM analysis for the given limit state equation for each lap. (input: distributions and its parameters / output: chart of failure probability for each lap)

import csv
import numpy as np
from scipy import stats
import pandas
import seaborn as sns
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
# Custom libs
from csvdata import getdata
from forces import (dyn, bolt, splitleg, intforces)
from stress import (stress, swt, morrow)


readdata = 'no'
filename = 'skid-sta.csv'
wheelpos = 'rear right'
leg = 'A'
cts = {
        'Cdrag': 1.76,         # [-]
        'Cdownforce': 3.9,     # [-]
        'air density': 1.225,  # [kg/m^3]
        'frontal area': 1.46,  # [m^2]
        'mass': 263.0,         # [kg]
        'gear equiv inertia': [1.269, 1.147, 1.106, 1.083, 1.069],  # [-]
        'distance from gc to rear axle': 0.803,     # [m]
        'height of gravity center': 0.262,          # [m]
        'car track': 1.223,     # [m]
        'wheelbase': 1.570,     # [m]
        'gravity': 9.81         # [m/s^2]
        }
ctsw = {
        'tire radius': (9 * 25.4/1000),     # [m]
        'tire width': 0.170,                # [m]
        'wheel center outer radius': 0.087, # [m]
        'wheel center inner radius': 0.040, # [m]
        'wheel center offset': 0.060,       # [m]
        'wheel beam curvature': 0.057,      # [m]
        'wheel beam bolt offset': 0.013,    # [m]
        'wheel beam angle range': 27,       # [deg]
        'wheel beam angle start': 3         # [deg]
        }
ctsm = {  # Aluminum 7075-T6
#        'fatigue strenght': 1.576e9          # [Pa] from [Glinka and Ince, 2011]
        'fatigue strenght': 1.303e9          # [Pa] from [Boyer's' Atlas]
        }


def section(angle):
    bs, hs = 6.6/1000, 6.9/1000   # [m]
    be, he = 12.0/1000, 10.3/1000  # [m]
    angs, ange = ctsw['wheel beam angle start'], ctsw['wheel beam angle range']
    b = (angle - angs)/(ange - angs) * (be - bs) + bs
    h = (angle - angs)/(ange - angs) * (he - hs) + hs
    return [b, h]


# Discretization of wheel rotation
angles = np.linspace(0, 2*np.pi, 90)

if readdata == 'yes':
    with open('fatigue-sigma.csv', 'w', newline='') as f:
        writer = csv.writer(f)

        # Previous inspection of fatigue stress on the positions of beam
        # result in a peak at 7º, which is near the observed failure.
        pos = 7

        dleft, dright = getdata(filename)

        for dturn in [dleft, dright]:
            fatsigma = []
            for index, datarow in dturn.iterrows():
                sigma = []
                Ftire = dyn(wheelpos, datarow, cts)
                for angle in angles:
                    Fbolt = bolt(Ftire, angle, ctsw)
                    Fleg = splitleg(wheelpos, leg, Fbolt, ctsw)
                    Fint = intforces(Fleg, pos, ctsw)
                    sigma.append(stress(Fint, section(pos), ctsw['wheel beam curvature']))
                fatsigma.append(morrow(max(sigma), min(sigma), ctsm['fatigue strenght']))

            # Preview distribution plots for data
            sns.distplot(fatsigma, kde=False, rug=True, fit=stats.lognorm)  # floc=0 is NOT set in seaborn!
            plt.show()
            sns.distplot(fatsigma, kde=True, fit=stats.norm)
            plt.show()

            # Write calculated fatigue equivalent stress in to file
            writer.writerow(fatsigma)

elif readdata == 'no':
    fatsigma = []
    with open('fatigue-sigma.csv', newline='') as f:
        reader = csv.reader(f, quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            fatsigma.append(row)

    for idrow, row in enumerate(fatsigma):
        # Get parameters for fitted distribution
        param = stats.lognorm.fit(row, floc=0)  # floc=0 sets lognorm to 'bound' zero
        print(param)  # Check scipy docs for specific parameters of lognorm

        # Reconstruct lognormal distribution
        arg = param[:-2]
        x = np.linspace(0, max(row), len(row))
        pdf_fitted = stats.lognorm.pdf(x, loc=param[-2], scale=param[-1], *arg)

        # Plot data (density) histogram vs fitted distribution
        plt.hist(row, bins=50, density=True, color='#999999')
        plt.plot(x, pdf_fitted, color='black')
        plt.xlabel('Tensão de fadiga '+('à esq.' if idrow == 0 else 'à dir.')+'[Pa]')
        plt.ylabel('Densidade de frequência')
        plt.show()
        plt.clf()

else:
    print("The 'readdata' parameter was not recognized. Check program input.")
