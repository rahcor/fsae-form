# Script to fit lognorm distribution in normal distributed data

import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


data = [[225.8/1000, 0.475/1000],
        [9.1, 0.15]]

for rvp in data:
    row = np.random.normal(rvp[0], rvp[1], 10000)

    # Get parameters for fitted distribution
    param = stats.lognorm.fit(row, floc=0)  # floc=0 sets lognorm to 'bound' zero
    print(param)  # Check scipy docs for specific parameters of lognorm

    # Reconstruct lognormal distribution
    arg = param[:-2]
    x = np.linspace(0, max(row), 1000)
    pdf_fitted = stats.lognorm.pdf(x, loc=param[-2], scale=param[-1], *arg)

    # Plot data (density) histogram vs fitted distribution
    plt.hist(row, bins=50, density=True, color='#999999')
    plt.plot(x, pdf_fitted, color='black')
    plt.show()
    plt.clf()
