# Set of functions to calculate forces from data in skid pad.
# dyn: forces acting on GC (dynamics of mass point) and tire contact point
# bolt: forces acting on each bolt interface of wheel center
# splitleg: forces acting on each leg of wheel center
# intforces: internal forces of leg

import sys
from math import (sin, cos, pi, sqrt, atan, radians, degrees)


def dyn(wheelpos, tdata, cts):
    # Assignments to local variables
    ax, ay = tdata["G Force Long"], tdata["G Force Lat"]
    if "rear" in wheelpos:
        v = (tdata["Wheel Speed RR"] if "right" in wheelpos else tdata["Wheel Speed RL"])
    else:
        v = (tdata["Wheel Speed FR"] if "right" in wheelpos else tdata["Wheel Speed FL"])
    gearid = int(tdata["Gear Position Sensor"]) - 1
    gb = (cts['gear equiv inertia'][gearid] if (gearid >= 0) else 1)
    Cdr, Cdn, rho, area = cts['Cdrag'], cts['Cdownforce'], cts['air density'], cts['frontal area']
    g, m = cts['gravity'], cts['mass']
    dcgr, hcg, e, b = cts['distance from gc to rear axle'], cts['height of gravity center'], cts['car track'], cts['wheelbase']

    # Aerodynamic forces
    A = Cdr * 0.5 * rho * v**2 * area
    D = A * Cdn/Cdr

    # Forces on GC
    N = m * g + D
    L = A + m * gb * ax
    T = m * ay

    # Forces on tire contact point given a selected wheel position
    def Ntfunc(wpos):
        return 0.5 * (N * ((1 - dcgr/e) if "rear" in wpos else (dcgr/e))
              + m * ax * (1 if "rear" in wpos else -1) * hcg/e
              + m * ay * (1 if "right" in wpos else -1) * hcg/b)
    Nt = Ntfunc(wheelpos)
    if L > 0:  # acelerating
        if "front" in wheelpos:
            Lt = 0
        else:  # rear wheel
            Lt = L * Nt/(Ntfunc("rear right") + Ntfunc("rear left"))
    else:
        Lt = L * Nt/N
    Tt = T * Nt/N

    return [Nt, Lt, Tt]


def bolt(Ftire, dl, cts, fi=[], m=6):
    # dl = angle of bolt for which forces will be calculated
    #      angle starts from the line between wheel center and the projection of tire contact point on wheel plane; angle increases as wheel rotation occur.
    # fi = list of indexes of failed bolts starting from zero
    # m = total number of bolts used

    # Bolt forces are calculated from transposition of forces on tire contact point to the center of wheel center offset diameter. This implies in 'generation' of equivalent momentums at the same center.
    # The force on each bolt is then calculated by an evenly division of forces to every bolt plus an delta of force intended to equilibrate the momentums.

    # Assignments to local variables
    N, L, T = Ftire[0], Ftire[1], Ftire[2]
    Rt, Rw = cts["tire radius"], cts["wheel center outer radius"]
    tr = cts["tire width"]/2 - cts["wheel center offset"]

    # sqsum of sine and cossine for every bolt position
    smsin, smcos = 0, 0
    ind = []  # indexes of each bolt
    for b in range(m):
        # ind.append(b) if (b not in fi) else pass
        if b not in fi:
            ind.append(b)
    for i in ind:
        smsin += sin(dl + i * 2 * pi/m)**2
        smcos += cos(dl + i * 2 * pi/m)**2

    # Calculation of delta forces for the momentums 'generated'
    DFMz = - L * tr * sin(dl) / (Rw * smsin)
    DFMx = (T * Rt + N * tr) * cos(dl) / (Rw * smcos)
    DFMy = L * Rt / (Rw * (m-len(fi)))

    # Calculation of bolt forces: Force/nºbolts + Deltas
    Ft = T/(m-len(fi)) - DFMz + DFMx
    Fl = L * cos(dl)/(m-len(fi)) - N * sin(dl)/(m-len(fi)) + DFMy
    Fn = - N * cos(dl)/(m-len(fi)) - L * sin(dl)/(m-len(fi))

    return [Fn, Fl, Ft]


def splitleg(wheelpos, leg, Fbolt, cts, m=6):
    # Leg forces on the plane of wheel are calculated by summing vectors
    # Leg forces normal tto the plane are evenly distributed in the two legs

    # Assignments to local variables
    Fn, Fl, Ft = Fbolt[0], Fbolt[1], Fbolt[2]
    Ri, Re = cts['wheel center inner radius'], cts['wheel center outer radius']

    q = pi/m  # the angle of the landing of legs on the inner diameter
    p = atan(sin(q)/( (Re/Ri) - cos(q) ))  # half of angle between the legs measured at outer diameter (bolt position)

    # The relative direction of transversal (normal) force to wheel direction is reversed depending on left/right wheel
    signT = (1 if "right" in wheelpos else -1)
    Gt = signT * Ft * (1 if "failed" in leg else 0.5)

    if "failed" in leg:
        Gn = Fn * cos(p) + (1 if ("B" in leg) else -1) * Fl * sin(p)
    else:
        Gn = (Fn * sin(p) + (1 if ("A" in leg) else -1) * Fl * cos(p))/sin(2*p)

    return [Gn, Gt]


def intforces(Fleg, t, cts):
    # t = angular position of the beam on its plane of curvature for which internal forces will be calculated

    # Assignments to local variables
    Gn, Gt = Fleg[0], Fleg[1]
    R, tmax, ofst = cts['wheel beam curvature'], cts['wheel beam angle range'], cts['wheel beam bolt offset']
    if t > tmax:
        print("Theta must not be greater than t maximum.\nProgram will exit.")
        sys.exit()

    # Calculation of forces usign Newton's 2nd law
    N = Gn * cos(t) - Gt * sin(t)
    V = Gn * sin(t) + Gt * cos(t)
    M = Gn * R * (1-cos(t)) + Gt * (R * sin(t) + ofst)

    return [N, V, M]
