import scipy.stats as st
import numpy as np
import matplotlib.pyplot as plt
import random as rd


def beckpdf_lognorm(x, plambda, xi):
    out = 1 / (xi*x*np.sqrt(2*np.pi))
    power = -0.5 * ((np.log(x)-plambda) / xi)**2
    return out*np.exp(power)


# sample_size = 1000
rd.seed(1)

# x = [rd.uniform(0, 20) for _ in range(sample_size)]
x = np.arange(0.0, 5.0, 0.01)
sample_size = int((5-0)/0.01)

plambda=1
xi=1

y_sp = [st.lognorm.pdf(x[i], xi, 0, np.exp(plambda)) for i in range(sample_size)]
y_bk = [beckpdf_lognorm(x[i], plambda, xi) for i in range(sample_size)]
y_dif = [ y_sp[i]-y_bk[i] for i in range(sample_size)]

fig, axs = plt.subplots(3, 1, sharex=True)
# Remove horizontal space between axes
#fig.subplots_adjust(hspace=0)

# Plot each graph, and manually set the y tick values
axs[0].plot(x, y_sp)
axs[1].plot(x, y_bk)
axs[2].plot(x, y_dif)

plt.show()
