# Open csv file previosly formatted, parse data and return vector of filtered and selected columns from dataset.

import csv
import pandas as pd
import numpy as np


def getdata(filename):
    # Open and read the dataset as a dictionary of float64 data
    df = None
    with open(filename, newline='') as f:
        reader = csv.DictReader(f)
        df = pd.DataFrame(reader, dtype=np.float64)

    # Select columns from the dataset
    cols = ["Time", "Distance", "G Force Lat", "G Force Long", "Wheel Speed FL", "Wheel Speed FR", "Wheel Speed RL", "Wheel Speed RR", "Gear Position Sensor"]
        # units = s, m, G(*2), km/h(*4), -
    dsel = df.filter(items=cols)

    # Convert units: (G -> m/s^2) and (km/h -> m/s)
    dsel['Gear Position Sensor'] = dsel['Gear Position Sensor'].astype(int)
    for ele in ["G Force Lat", "G Force Long"]:
        dsel[ele] = dsel[ele] * 9.81
    for ele in ["Wheel Speed FL", "Wheel Speed FR", "Wheel Speed RL", "Wheel Speed RR"]:
        dsel[ele] = dsel[ele] / 3.6

    # Filter the dataset
    dfilt = dsel[dsel['Wheel Speed RL'] > 30.0/3.6]

    # Separate data for left and right turn
    dleft = dfilt[dfilt['G Force Lat'] > 0]
    dright = dfilt[dfilt['G Force Lat'] < 0]

    return dleft, dright
