# FORM python 3.5.3 script
# Reliability problem solved: fatigue in an automotive wheelcenter
# RESULTS
#	nv	beta	pf=phi(-beta)
#	--	-----	----
#	1	1.207	0.11
#	5	0.810	0.21
#	10	0.640	0.26
#	20	0.470	0.32
#	30	0.372	0.35
#	50	0.248	0.36
#	75	0.150	0.44
#	100	0.081	0.47
#	130	0.018	0.49

import random
import numpy as np                  # version 1.17.4
import scipy.stats as st            # version 1.4.0; for statistical distributions
from numdifftools import Gradient   # version 0.9.39; for gradient calculation
import matplotlib.pyplot as plt     # version 3.0.3
import sys                          # version 3.5.3 on linux kernel 4.9.0-11-amd64; for exit program on error


## Data for example 5 of page 102 on the book "Confiabilidade e segurança das estruturas" by BECK,A.T.
#def limstate(x): return x[0] - x[1] - x[2]
#X = [['lognormal', 3.3289, 0.4328],
#     ['normal', 1.05, 0.105],
#     ['gumbel_max', 1.0, 0.25]]
#Xparam = []
#for rv in X:  # Convertion of Beck's book parameters to Scipy parameters
#    if rv[0] == 'normal': Xparam.append([rv[1], rv[2]])
#    elif rv[0] == 'lognormal':
#        xi = np.sqrt(np.log( 1+(rv[2]/rv[1])**2 ))  # log is in neperian base in numpy
#        plambda = np.log(rv[1]) - 0.5 * xi**2
#        Xparam.append([plambda, xi])
#    elif rv[0] == 'gumbel_max':
#        beta = np.pi/(rv[2] * np.sqrt(6))
#        un = rv[1] - np.euler_gamma/beta
#        Xparam.append([un, beta])
#    else:
#        sys.exit("The statistical distribution named '", rv[0], "' was not found. Please check its name. Or else the refered distribution may not be available in this program.")
#rho_x = [[1, 0, 0],
#         [0, 1, 0],
#         [0, 0, 1]]
#### END OF DATA


# LIMIT STATE FUNCTION
# The original limit state equation is the Palmgren-Miner linear damage model for fatigue. It was described here for skid-pad left and right turns only.
# The limit state function shown here is an alternate mathematical formulation of the original formula aiming a more linear function.
def limstate(x, nv=100, a=1.303e9, C=1.0):
#    return C * x[0] * (1./a)**(1./x[2]) - 2 * nv * x[1] * (1./x[3]**(1./x[2]) + 1./x[4]**(1./x[2]))  # This formula is lesser stable for convergence
    return C * x[0] - 2 * nv * x[1] * (1./(x[3]/a)**(1./x[2]) + 1./(x[4]/a)**(1./x[2]))
    #      C * rtire - 2 * nv * rskid * (1./(sigleft/a)**(1./b) + 1./(sigright/a)**(1./b))


# Random variables parameters
# [type, mean, stdev]
X = [['lognormal', 0.2258, 0.0021],   # tire radius
     ['lognormal', 9.099, 0.0165],    # skid pad radius
     ['normal', -0.1285, 0.0131],     # 'b' parameter of S-N fatigue equation
     ['lognormal', 3.928e8, 0.523],   # Equivalent fatigue stress for left turn
     ['lognormal', 6.491e7, 0.513]]   # Equivalent fatigue stress for right turn
Xparam = [[rv[1], rv[2]] for rv in X]  # Coping X to Xparam without first column
# Correlation matrix of random variables
rho_x = [[1, 0, 0, 0, 0],
         [0, 1, 0, 0, 0],
         [0, 0, 1, 0, 0],
         [0, 0, 0, 1, 0],
         [0, 0, 0, 0, 1]]

conv_beta = 1E-6      # convergence criteria (for delta_beta = beta_old - beta)
max_steps = 1E3       # max number of steps allowed in solver


def pdf(dtype, param, x):
    if   dtype == 'normal': return st.norm.pdf(x, param[0], param[1])
     # np.exp(param[0]) is used to match Beck's syntax for lognorm, otherwise (for Scipy) only param[0] should be used
#    elif dtype == 'lognormal': return st.lognorm.pdf(x, param[1], 0, np.exp(param[0]))
    elif dtype == 'lognormal': return st.lognorm.pdf(x, param[1], 0, param[0]) 
    elif dtype == 'gumbel_max': return st.gumbel_r.pdf(x, param[0], 1./param[1])
    else: sys.exit("The statistical distribution named '", dtype, "' was not found. Please check its name. Or else the refered distribution may not be available in this program.")

def cdf(dtype, param, x):
    if   dtype == 'normal': return st.norm.cdf(x, param[0], param[1])
    # np.exp(param[0]) is used to match Beck's syntax for lognorm, otherwise (for Scipy) only param[0] should be used
#    elif dtype == 'lognormal': return st.lognorm.cdf(x, param[1], 0, np.exp(param[0]))
    elif dtype == 'lognormal': return st.lognorm.cdf(x, param[1], 0, param[0])
    elif dtype == 'gumbel_max': return st.gumbel_r.cdf(x, param[0], 1./param[1])
    else: sys.exit("The statistical distribution named '", dtype, "' was not found. Please check its name. Or else the refered distribution may not be available in this program.")

def ppf(dtype, param, x):
    if   dtype == 'normal': return st.norm.ppf(x, param[0], param[1])
    # np.exp(param[0]) is used to match Beck's syntax for lognorm, otherwise (for Scipy) only param[0] should be used
#    elif dtype == 'lognormal': return st.lognorm.ppf(x, param[1], 0, np.exp(param[0]))
    elif dtype == 'lognormal': return st.lognorm.ppf(x, param[1], 0, param[0])
    elif dtype == 'gumbel_max': return st.gumbel_r.ppf(x, param[0], 1./param[1])
    else: sys.exit("The statistical distribution named '", dtype, "' was not found. Please check its name. Or else the refered distribution may not be available in this program.")


# Calculation of correlation matrix in equivalent normal space:
# using the approximation rho_z = rho_x
rho_z = rho_x

# Calculation of Jacobian matrix of transformation Z<->Y (Equiv. normal<->Standard normal)
L = np.linalg.cholesky(rho_z)
J_zy = L
J_yz = np.linalg.inv(J_zy)

# Build a set of starting points to obtain a clue about convergence to global minimum (design point)
x_0set = [ np.array([ppf(X[i][0], Xparam[i], random.uniform(0.2,0.8)) for i in range(len(X))]) for _ in range(10) ]  # Uniform was limited to [.2,.8] because non-linearity of limstate equation implied non convergence beyond this boundary
x_0set.append(np.array([rv[1] for rv in X]))

x_dpset = []
for x_0 in x_0set:
    print('x_0 = ', x_0)
    ### HLRF algorithm to find design point and reliability index (beta)
    # Declaring variables for plots
    plt_k = []
    plt_beta = []
    plt_g = []

    # Declaring variables
    k = 0  # counter of steps until reach convergence
    below_max_steps = True    # Loop initialization
    converged = False         # Loop initialization
    first_step = True
    x_k = x_0  # x_k is the point in the original space

    #np.seterr('raise')
    while (not converged) and below_max_steps:
        if not first_step:
            k += 1  # increment counter

            # Calculate next point in normal std space
            y_k = - alpha * (beta + limstate(x_k)/np.linalg.norm(gradlimstate_yk))

            # Convert this point to original space
            x_k = (J_xy @ y_k + meanneq).A1
            # Note: The call '.A1' flat the (3,1) matrix to a (3) array

        # Obtain the equivalent normal point by preserving original cumulative probability
        # Note: ppf (percent-point function) is the inverse cumulative distribution function
        z_k = np.array([st.norm.ppf(cdf(X[i][0], Xparam[i], x_k[i])) for i in range(len(X))])

        # Obtain the normal equivalent standard deviation
        stdvneq = np.array([(st.norm.pdf(z_k[i])/pdf(X[i][0], Xparam[i], x_k[i])) for i in range(len(X))])

        # Obtain the normal equivalent mean
        meanneq = x_k - z_k * stdvneq

        # Calculate the Jacobian matrix of transformation X<->Z (Original <-> Equivalent Normal)
        # Note: J_xz = D_neq
        D_neq = np.matrix([[(stdvneq[i] if i == j else 0) for i in range(len(X))] for j in range(len(X))])

        # Calculate the Jacobian matrix of transformation X<->Y (Original <-> Standard Normal)
        J_xy = D_neq @ J_zy
        J_yx = np.linalg.inv(J_xy)

        # Convert the point back to normal standard space using updated matrix of transformation J_yx
        y_k = (J_yx @ (x_k - meanneq)).A1
        # Note: The call '.A1' flat the (3,1) matrix to a (3) array

        # Evaluate new value of reliability index
        if not first_step:
            beta_old = beta
        beta = np.linalg.norm(y_k)

        # Obtain the gradient of limit state function at the point
        gradlimstate_xk = Gradient(limstate)(x_k)  # In the original space
        gradlimstate_yk = (gradlimstate_xk @ J_xy).A1  # In the std normal space
        # Calculate the unit vector of the gradient
        alpha = gradlimstate_yk/np.linalg.norm(gradlimstate_yk)

        if not first_step:
            # Check convergence and algorithm failure criteria
            if abs(beta - beta_old) < conv_beta:
                converged = True
            if k >= max_steps:
                below_max_steps = False
        else:
            first_step = False

        print("k=", k, "; beta= ", np.around(beta, decimals=3), "@ x = ", np.around(x_k, decimals=3), "with limstate=", np.around(limstate(x_k), decimals=5))

        # Assign values for plots
        plt_k.append(k)
        plt_beta.append(beta)
        plt_g.append(limstate(x_k))
    ### end of HLRF

    if converged:
        print("\nHLRF have sucessfully coverged in ", k, " steps.")
        print("beta= ", np.around(beta, decimals=5), "@ x* = ", np.around(x_k, decimals=3))
        x_dpset.append(x_k)
        print("sensibility of factors:", np.sign(alpha) * alpha**2, "\n")
    elif not below_max_steps:
        print("\nHLRF have not coverged in defined max_steps!")
        print("Please check if either max_steps is too low, convergence criteria is too narrow or the problem is ill formed.\n")
        x_dpset.append(None)
    else:
        print("\nHLRF have escaped while loop!")
        sys.exit("Please check program code.\n")
    fig, axs = plt.subplots(2, 1, sharex=True)
    axs[0].plot(plt_k, plt_g, 'o-')
    axs[1].plot(plt_k, plt_beta, 'o-')
    plt.show()
### END OF CALCULATIONS FOR MULTIPLE x_0

print('Desing point for several x_0 inputs:')
print('[x_0]\t\t\t[x_dp]')
for i, x_dpi in enumerate(x_dpset):
    x_0f = ["%.3E"%x_0j for x_0j in x_0set[i]]  # Format x_0 output
    if x_dpi is not None:  # Format x_dp output
        x_dpf = ["%.3E"%x_dpj for x_dpj in x_dpi]
    else:
        x_dpf = None
    print(x_0f, x_dpf)


# Check if solutions for the entire x_0 set are equal so the design point found appears to be the global converged solution
looks_global = True
for i in x_dpset:
    for j in x_dpset:
        if (j is not None) and (i is not None):
            if (abs(np.linalg.norm((i - j)/j)) > conv_beta):
                looks_global = False
        else:
            looks_global = False
print('Conclusion: when tested for serveral x_0, convergence', ('DO NOT' if looks_global is False else ''), 'appears to be global.\n')
