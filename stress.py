# Functions to compute curved beam stress, SWT equivalent stress and limit state function

import sys
from math import (log, fabs)


def stress(Fint, crosssection, R, csig=None):
    # Stress on curved beam. From [Roark's Formulas for Stress and Strain]
    # csig is the height from centroid line in which stress will be calculated
    N, M = Fint[0], Fint[2]
    b, h = crosssection[0], crosssection[1]

    if csig is None:
        csig = -0.5*h
    elif fabs(csig) > 0.5*h:
        print("csig cannot be greater than half of section height.\nExitign program.")
        sys.exit()
    rloc = R + csig

    area = b * h
    aream = b * log((R + 0.5*h)/(R - 0.5*h))

    # Normal stress
    signormal = N/area

    # Flexural stress
    sigflexnum = M * (area/rloc - aream)    # numerator
    sigflexden = area * (R * aream - area)  # denominator
    sigflex = sigflexnum/sigflexden

    # Resultant stress will be the sum of normal and flexural stresses
    return signormal + sigflex


def swt(sigmax, sigmin):
    # Equivalent alternate SWT stress
    if sigmin > sigmax:
        print("sigmin must be placed after sigmax when calling function sigeq.\nExiting program.")
        sys.exit()

    deltasig = sigmax - sigmin
    if deltasig < 0 or sigmax < 0:
        print("A compressive mean stress was detected for SWT. Assuming zero.")
        return 0
    else:
        return (sigmax * 0.5 * deltasig)**(1./2)


# SWT don't allow compressive mean stresses, which were found to occur often. Therefore an alternate equivalent stress is needed.
def morrow(sigmax, sigmin, sigf):
    deltasig = sigmax - sigmin
    sigm = sigmin + 0.5 * deltasig
    
    return 0.5 * deltasig * 1/(1 - sigm/sigf)
