# Excerpt from main script placed after getdata() and angles discretization
# It was used for Inspection of fatigue stress on the positions of beam
# Result: Found out a peak at 7º, which is near the observed failure.


# Libs used:
#import random
#import numpy as np
#import pandas
#import seaborn as sns
#import matplotlib.pyplot as plt
## Custom libs
#from forces import (dyn, bolt, splitleg, intforces)
#from stress import (stress, swt, morrow, limstate)


# Discretization of positions along the curved beam
positions = np.linspace(ctsw['wheel beam angle start'], ctsw['wheel beam angle range'], ctsw['wheel beam angle range']-ctsw['wheel beam angle start']+1) #positionstot
#positionssmpl = [positions[i] for i in sorted(random.sample(range(len(positions)), 4))]
for dturn in [dleft, dright]:
    posofmaxfat = []
    dturnsample = dturn.sample(n=1000)
    for index, datarow in dturnsample.iterrows():
        sigma = [[] for _ in range(len(positions))]  # for every position will be appended a list of stress of a wheel turn
        Ftire = dyn(wheelpos, datarow, cts)
        angsmpl = [angles[i] for i in sorted(random.sample(range(len(angles)), 4))]
        for angle in angles:
            Fbolt = bolt(Ftire, angle, ctsw)
            Fleg = splitleg(wheelpos, leg, Fbolt, ctsw)
            for idpos, pos in enumerate(positions):
                Fint = intforces(Fleg, pos, ctsw)
                sigma[idpos].append(stress(Fint, section(pos), ctsw['wheel beam curvature']))
        fatsigma = []
        for idpos, pos in enumerate(positions):
            fatsigma.append(morrow(max(sigma[idpos]), min(sigma[idpos]), ctsm['fatigue strenght']))
        posofmaxfat.append(positions[fatsigma.index(max(fatsigma))])
#        print(datarow)
#        plt.plot(angles, aux)
#        plt.clf()
#        for idpos, pos in enumerate(positions):
#            plt.subplot(len(positions), 1, idpos+1)
#            plt.plot(angles, sigma[idpos])
#            plt.ylabel('pos ' + str(int(pos)))
#        plt.show()
    print(posofmaxfat)
    sns.distplot(posofmaxfat, kde=False)
    plt.show()
